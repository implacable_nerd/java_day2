

class Customer {
	private String FirstName;
	private String LastName;
	private	double Balance;
	private String CustomerCode;
	
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public double getBalance() {
		return Balance;
	}
	public void setBalance(double balance) {
		Balance = balance;
	}
	public String getCustomerCode() {
		return CustomerCode;
	}
	public void setCustomerCode(String customerCode) {
		CustomerCode = customerCode;
	}
	
	public Customer() {
		FirstName = "";
		LastName = "";
		Balance = 0;
		CustomerCode = "";
	}
	public Customer(String firstName, String lastName, double balance, String customerCode) {
		FirstName = firstName;
		LastName = lastName;
		Balance = balance;
		CustomerCode = customerCode;
	}
	@Override
	public String toString() {
		return "Customer:\n\tFirstName:\t" + getFirstName() + "\n\tLastName:\t" + getLastName() + "\n\tBalance:\t"
				+ getBalance() + "\n\tCustomerCode:\t" + getCustomerCode();
	}
}

public class Bank{
	public static void main(String[] args) {
		Customer c = new Customer("Alok", "Patro", 5000.00, "1CB23A");
		System.out.println(c);
	}
}
